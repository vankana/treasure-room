/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : tourist

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 12/06/2021 22:47:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '账号',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '昵称',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '姓名',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `sex` int NULL DEFAULT 0 COMMENT '性别 0.未知 1.男 2.女',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '电话号码',
  `headImage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '头像',
  `openId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '微信openId',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '密码',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态 0.禁止使用 1.待审核 2.正常使用 ',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `createTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `lastLoginTime` datetime NULL DEFAULT NULL COMMENT '上次登录沈佳妮',
  `deleted` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, '15228297669', '系统管理员', 'admin', 'test1@baomidou.com', 0, '110', NULL, NULL, '78267128d98d20cb07b1762a6e80e1bc', '0', NULL, '2021-06-09 22:00:46', '2021-06-11 14:41:32', 0);

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `articleId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '文章ID',
  `briefId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '摘要ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文章标题',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文章内容',
  `deleted` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`articleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of article
-- ----------------------------

-- ----------------------------
-- Table structure for article_brief
-- ----------------------------
DROP TABLE IF EXISTS `article_brief`;
CREATE TABLE `article_brief`  (
  `articleId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '文章ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文章标题',
  `type` int NULL DEFAULT 0 COMMENT '文章状态 0 存档 1 发布',
  `state` int NULL DEFAULT NULL COMMENT '文章类型 0 原创 1 转载',
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文章章摘要图片',
  `brief` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文章简介',
  `views` bigint NULL DEFAULT 0 COMMENT '文章浏览量',
  `shares` bigint NULL DEFAULT 0 COMMENT '文章转发量',
  `likes` bigint NULL DEFAULT 0 COMMENT '文章点赞量',
  `createTime` datetime NULL DEFAULT NULL COMMENT '文章创建时间',
  `modifyTime` datetime NULL DEFAULT NULL COMMENT '修改、发布时间',
  `authorId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '作者Id',
  `authorName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '作者昵称',
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文章标签',
  `sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文章分类名称',
  `sortId` int NULL DEFAULT 0 COMMENT '文章分类Id',
  `deleted` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`articleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of article_brief
-- ----------------------------

-- ----------------------------
-- Table structure for article_label
-- ----------------------------
DROP TABLE IF EXISTS `article_label`;
CREATE TABLE `article_label`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `num` bigint NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of article_label
-- ----------------------------

-- ----------------------------
-- Table structure for article_sort
-- ----------------------------
DROP TABLE IF EXISTS `article_sort`;
CREATE TABLE `article_sort`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of article_sort
-- ----------------------------
INSERT INTO `article_sort` VALUES (1, '大新闻');
INSERT INTO `article_sort` VALUES (2, '动态');
INSERT INTO `article_sort` VALUES (4, '生活');
INSERT INTO `article_sort` VALUES (6, '大事件');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '上级名称',
  `sId` int NULL DEFAULT 0 COMMENT '上级编号',
  `type` int NULL DEFAULT 0 COMMENT '类型 0 未知 1 游客展示页面 2 管理员控制页面',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '地址',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图标',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '姓名',
  `sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '排序 越大越前',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '系统', 0, 2, 'm', '', '权限管理', '0');
INSERT INTO `menu` VALUES (2, '系统', 0, 2, '/', '', '系统管理', '0');
INSERT INTO `menu` VALUES (3, '系统管理', 2, 2, 'menu/list.html', '', '导航配置', '0');
INSERT INTO `menu` VALUES (4, '系统', 0, 2, '/', '', '官网项目管理', '0');
INSERT INTO `menu` VALUES (5, '官网项目管理', 4, 2, 'project/project.html', '', '项目配置', '0');
INSERT INTO `menu` VALUES (6, '官网项目管理', 4, 2, 'project/module.html', '', '项目模块配置', '0');
INSERT INTO `menu` VALUES (7, '官网项目管理', 4, 2, 'project/show.html', '', '项目模块内容', '0');
INSERT INTO `menu` VALUES (8, '系统', 0, 2, '/', '', '内容管理', '0');
INSERT INTO `menu` VALUES (9, '内容管理', 8, 2, 'article/release.html', '', '发布文章', '0');
INSERT INTO `menu` VALUES (10, '内容管理', 8, 2, 'article/uploading.html', '', '上传资源', '0');
INSERT INTO `menu` VALUES (11, '内容管理', 8, 2, 'article/classify.html', '', '分类管理', '0');
INSERT INTO `menu` VALUES (12, '内容管理', 8, 2, 'article/essay.html', '', '文章管理', '0');
INSERT INTO `menu` VALUES (13, '内容管理', 8, 2, 'article/resource.html', '', '资源管理', '0');
INSERT INTO `menu` VALUES (14, '内容管理', 8, 2, 'article/discuss.html', '', '评论管理', '0');
INSERT INTO `menu` VALUES (15, '系统', 0, 1, 'index.html', '', '首页', '0');
INSERT INTO `menu` VALUES (16, '系统', 0, 1, 'about.html', '', '关于', '0');
INSERT INTO `menu` VALUES (17, '系统', 0, 1, 'sort.html', '', '分类', '0');
INSERT INTO `menu` VALUES (18, '系统', 0, 1, 'show.html', '', '展示', '0');
INSERT INTO `menu` VALUES (19, '系统', 0, 1, 'document.html', '', '归档', '0');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `logLevel` int NULL DEFAULT 0 COMMENT '等级',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `createTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '内容',
  `type` int NULL DEFAULT 0 COMMENT '用户类型 0游客  1用户  2管理员  3系统',
  `userId` bigint NULL DEFAULT NULL COMMENT '用户id',
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户名',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '参数',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'ip',
  `deleted` int NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1, 0, '登录', '2021-06-11 10:31:25', '登录系统', 2, 1, 'admin', 'Admin{id=\'1\', name=\'admin\', nickname=\'系统管理员\', account=\'15228297669\', sex=0, phone=\'110\', headImage=\'null\', openId=\'null\', password=\'78267128d98d20cb07b1762a6e80e1bc\', status=0, email=\'test1@baomidou.com\', createTime=Wed Jun 09 22:00:46 CST 2021, lastLoginTime=Fri Jun 11 10:31:25 CST 2021, remark=\'null\', deleted=0}', NULL, 0);
INSERT INTO `sys_log` VALUES (2, 0, '登录', '2021-06-11 14:41:32', '登录系统', 2, 1, 'admin', 'Admin{id=\'1\', name=\'admin\', nickname=\'系统管理员\', account=\'15228297669\', sex=0, phone=\'110\', headImage=\'null\', openId=\'null\', password=\'78267128d98d20cb07b1762a6e80e1bc\', status=0, email=\'test1@baomidou.com\', createTime=Wed Jun 09 22:00:46 CST 2021, lastLoginTime=Fri Jun 11 14:41:31 CST 2021, remark=\'null\', deleted=0}', NULL, 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '姓名',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `deleted` int NULL DEFAULT 0,
  `createTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modifyTime` datetime NULL DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `sex` int NULL DEFAULT 0,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `headImage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `openId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `lastLoginTime` datetime NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'Jone', 18, 'test1@baomidou.com', 0, '2021-04-09 14:40:59', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (2, 'Jack', 20, '修改一下！', 0, '2021-04-09 14:41:00', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (3, 'Tom', 28, 'test3@baomidou.com', 0, '2021-04-09 14:41:01', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (4, 'Sandy', 21, 'test4@baomidou.com', 0, '2021-04-09 14:41:01', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (5, 'Billie', 24, 'test5@baomidou.com', 0, '2021-04-09 14:41:02', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (60, '小王编号39', 39, '小王修改一下！', 0, '2021-04-09 14:40:48', '2021-04-09 10:33:02', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (61, '小王编号17', 17, NULL, 0, '2021-04-09 14:41:04', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (62, '小王编号15', 15, NULL, 0, '2021-04-09 14:41:05', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (63, '小王编号68', 68, '添加一个', 0, '2021-04-09 14:41:06', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (64, '小王小王编号90', 90, '小王添加一个', 0, '2021-04-09 14:41:07', '2021-04-09 14:40:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
