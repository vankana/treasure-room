# treasure-room

## 参考

### 源码

- [Spring Cloud Alibaba 微服务教程](https://github.com/mtcarpenter/spring-cloud-learning)
- [山间木匠 / mall-cloud-alibaba](https://gitee.com/mtcarpenter/mall-cloud-alibaba?_from=gitee_search)
- [MarkerHub的eblog](https://gitee.com/markerhub/eblog)

### 网站

- [amarts](https://camarts.app/?bN) 是翁天信 Dandy Weng 独立设计与开发的个人影集，用于展示自己在环游世界的旅途中所拍摄的摄影作品。
- [MyBatis-Plus](https://mp.baomidou.com/) 技术网站
- [MarkerHub](https://www.markerhub.com/) 开源项目学习网

### 博客
- [KENJI ENDO](http://kenjiendo.com/) 将音乐与设计结合到一起的个人网站
- [二次元技术宅 -小游](https://xiaoyou66.com/) 
- [仿 shell 界面](https://axton.cc/)
- [吕靖 blog](https://blog.jimmylv.info/)
- [YEVPT blog](https://www.yevpt.com/)
## 前言

通过 `treasure-room` 学习 spring cloud alibaba

## 项目介绍

treasure-room 是一套基于 spring cloud alibaba 的微服务博客系统。
采用了spring cloud alibaba 、MyBatis等核心技术。
博客系统包含首页门户、文章推荐、文章搜索、作品展示、时间轴等模块。 
后台管理包括用户管理、文章管理、分类管理、标签管理、权限管理、设置等模块。
其他功能：生日提醒

## 演示地址

前端演示地址：http://127.0.0.1:8171/

后端演示地址：http://127.0.0.1:8170/  用户名:admin 密码:123456

管理端Druid监控：http://127.0.0.1:8170/druid/api.html 用户名:admin 密码:admin123
用户端Druid监控：http://127.0.0.1:8171/druid/api.html 用户名:admin 密码:admin123

## 技术栈

- 注册中心：Nacos
- 注册中心：MyBatis-Plus

## 组织架构

```
treasure-room
├─room-common    -- 工具类及通用代码
├─room-project   -- 业务
  ├─room-project-manage    -- 管理端模块服务，端口 8170
  ├─room-project-tourist   -- 用户端模块服务，端口 8171
```

