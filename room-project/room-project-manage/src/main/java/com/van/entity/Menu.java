package com.van.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * 导航菜单
 * @author VanKa (tel:15228297669 qq:2395073039)
 * @date 2021-04-15 10:22
 */
@Data
@TableName(value = "menu")
public class Menu {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 上级名称
     */
    @TableField(value = "sName")
    private String sName;
    /**
     * 上级编号
     */
    @TableField(value = "sId")
    private int sId;
    /**
     * 类型 0 全部 1父类 2子类
     */
    private int type;

    /**
     * 地址
     */
    private String url;
    /**
     * 图标
     */
    private String icon;
    /**
     * 排序 越大越前
     */
    private int sort;

    @TableField(exist = false)
    private List<Menu> data;
}
