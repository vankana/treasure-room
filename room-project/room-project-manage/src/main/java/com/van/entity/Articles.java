package com.van.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.math.BigInteger;
import java.util.Date;

/**
 * 文章
 * @author VanKa qq:2395073039
 * @date 2021/6/18 15:04
 */
@Data
@TableName(value = "articles")
public class Articles {

    //文章Id
    @TableId(value = "articlesId", type = IdType.ASSIGN_ID)
    private String articlesId;

    //文章类型 0 原创 1 转载
    private Integer type = 0;
    //文章状态 0 存档 1审核 2 发布
    private Integer state = 0;

    @TableField(value = "authorId")
    private String authorId;//作者ID
    @TableField(value = "authorName")
    private String authorName;//作者昵称

    //文章标签
    private String tag;
    //分类
    @TableField(value = "categoryName")
    private String categoryName;
    @TableField(value = "categoryId")
    private String categoryId;

    private String title;//文章标题
    private String picture;//文章头图
    private String brief;//文章简介

    @TableField(value = "contentType")
    private int contentType; //内容类型 1 markdown内容 2 html内容
    @TableField(value = "markdownContent")
    private String markdownContent;//markdown内容
    @TableField(value = "htmlContent")
    private String htmlContent;//html内容

    private Integer views = 0;//文章浏览量
    private Integer shares = 0;//文章转发量
    private Integer likes = 0;//文章点赞量
    private Integer comments = 0;//文章评论量

    @TableField(value = "createTime", fill = FieldFill.INSERT)
    private Date createTime = new Date(); //文章创建时间
    @TableField(value = "modifyTime", fill = FieldFill.UPDATE)
    private Date modifyTime = new Date();//修改、发布时间

    //逻辑删除
    @TableLogic
    private Integer deleted = 0;
}
