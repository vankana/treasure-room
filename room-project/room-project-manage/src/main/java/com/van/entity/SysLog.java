package com.van.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 日志
 */
@Data
@TableName(value = "sys_log")
public class SysLog implements Serializable{
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String title = Status.SYSTEM.name;//标题
    //内容
    private String content;

    //用户类型 0游客  1用户  2管理员  3系统
    private Integer type = 0;
    //用户id
    @TableField("userId")
    private Long userId;
    //用户名
    @TableField("userName")
    private String userName;
    //等级 0都可以看 1管理员看 2超级管理员看
    @TableField("logLevel")
    private Integer logLevel = 0;

    //参数
    private String params;

    //ip
    private String ip;

    //创建时间
    @TableField("createTime")
    private Date createTime = new Date();

    //逻辑删除
    @TableLogic
    private Integer deleted;

    /**
     * 初始化
     */
    public SysLog() {}
    public SysLog(Status status, String content, Object params, int type) {
        this.title = status.name;
        this.content = content;
        this.params = params.toString();
        this.type = type;
    }
    public SysLog(Status status, String content, Object params, int type,Admin admin) {
        this.title = status.name;
        this.content = content;
        this.params = params.toString();
        this.type = type;
        if (admin != null){
            this.type = 2;
            this.userId = admin.getId();
            this.userName = admin.getName();
        }
    }
    public SysLog(Status status, String content, Object params,Admin admin) {
        this.title = status.name;
        this.content = content;
        this.params = params.toString();
        this.type = 2;
        if (admin != null){
            this.userId = admin.getId();
            this.userName = admin.getName();
        }
    }



    /**
     * 添加用户信息
     */
    public void setUser(Admin admin) {
        this.type = 2;
        this.userId = admin.getId();
        this.userName = admin.getName();
    }

    public enum Status {
        SYSTEM("系统日志"),
        LOGIN("用户登录"),

        Add_Articles("添加文章"),
        Update_Articles("修改文章"),
        Del_Articles("删除文章"),

        Add_Articles_Category("添加文章分类"),
        Update_Articles_Category("修改文章分类"),
        Del_Articles_Category("删除文章分类"),

        Add_Menu("添加导航"),
        Update_Menu("修改导航"),
        Del_Menu("删除导航"),

        Up_Img("上传图片"),

        ERROR("错误");
        // 成员变量
        private final String name;
        // 构造方法
        private Status(String name) {
            this.name = name;
        }
    }
}
