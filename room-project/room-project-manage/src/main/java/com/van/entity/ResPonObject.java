package com.van.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/9 15:51
 */
@Data
public class ResPonObject  implements Serializable {
    /**
     * 数据
     */
    private Object data;
    /**
     * 查询数据总量
     */
    private int count;
    /**
     * 回复
     */
    private String msg = "";
    /**
     * 响应错误码
     */
    private String code = "0";
    /**
     *
     请求处理状态
     */
    private Status status;

    public enum Status {
        SUCCESS,//成功
        ERROR,//错误
        NOT_MONEY,//余额不足
        NOT_INTEGRAL,//积分不足
        NOT_LOGIN,//未登录
        NOT_LOGIN_NOT,//不允许重复登录
        NOT_FIND,//未找到
        EXCEPTION,//异常
        NOT_BALANCE,//余额不足
        NOT_AUTHOR,//无权限
        NOT_PASSWORD_ERROR,//支付密码错误
        SOCKET_NO_REGISTER//socket未注册
    }

    public ResPonObject() {
        this.status = Status.SUCCESS;
    }

    public ResPonObject(Object data) {
        this.status = Status.SUCCESS;
        this.data = data;
    }

    public ResPonObject(String msg,Status status) {
        this.status = status;
        this.msg = msg;
    }

    /**
     * 初始化 分页 数据
     * @param num
     * @param list
     */
    public ResPonObject(int num, List list) {
        if (num <= 0){
            this.status = Status.ERROR;
            this.msg = "查询数据为空";
        }else {
            this.status = Status.SUCCESS;
        }
        this.count = num;
        this.data = list;
    }


    /**
     * 添加or修改数据后返回
     * @param on 添加or修改数据后返回
     * @param data 数据
     * @param msg 失败原因
     */
    public ResPonObject(boolean on, Object data, String msg) {
        this.data = data;
        if (on){
            this.status = Status.SUCCESS;
        }else {
            this.status = Status.ERROR;
            this.msg = msg;
        }
    }
}
