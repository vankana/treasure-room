package com.van.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.math.BigInteger;
import java.util.Date;

/**
 * 文章分类
 * @author VanKa qq:2395073039
 * @date 2021/6/18 15:05
 */
@Data
@TableName(value = "articles_category")
public class ArticlesCategory {

    @TableId(value = "categoryId", type = IdType.ASSIGN_ID)
    private String categoryId;//分类id
    @TableField(value = "categoryName")
    private String categoryName;//分类名

    private int num = 0;//分类数量
    private String remark;//备注

    @TableField(value = "createTime", fill = FieldFill.INSERT)
    private Date createTime = new Date(); //创建时间
}
