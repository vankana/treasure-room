package com.van.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 文件上传记录类
 *
 * @author VanKa (tel:15228297669 qq:2395073039)
 * @date 2021-04-12 11:44
 */
@Data
@TableName(value = "up_file")
public class UpFile {
    //ID
    @TableId(value = "fileId", type = IdType.AUTO)
    private Integer fileId;
    //文件类型 0未知 1图片 2表格 3文档 4压缩包
    private int type;
    //ip
    private String ip;
    //服务器地址
    private String address;
    //url地址
    private String url;
    //上传人Id
    @TableField(value = "userId")
    private int userId;
    //上传人名称
    @TableField(value = "userName")
    private String userName;
    //上传文件名称
    @TableField(value = "fileName")
    private String fileName;
    //备注
    private String remark;
    //创建时间
    @TableField(value = "createTime", fill = FieldFill.INSERT)
    private Date createTime;
    //逻辑删除
    @TableLogic
    private Integer deleted;
    public UpFile(){
        this.createTime = new Date();
    }
    public UpFile(int type,String address,String remark){
        this.type = type;
        this.address = address;
        this.remark = remark;
        this.createTime = new Date();
    }
}
