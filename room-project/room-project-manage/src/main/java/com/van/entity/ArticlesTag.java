package com.van.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 文章标签
 * @author VanKa qq:2395073039
 * @date 2021/6/18 15:05
 */
@Data
@TableName(value = "articles_tag")
public class ArticlesTag {

    @TableId(value = "tagId", type = IdType.ASSIGN_ID)
    private String tagId;//文章标签id
    @TableField(value = "tagName")
    private String tagName;//文章标签名

    @TableField(value = "articlesId")
    private String articlesId;//文章Id
    private String title;//文章标题

    @TableField(value = "authorId")
    private String authorId;//作者ID
    @TableField(value = "authorName")
    private String authorName;//作者昵称

    @TableField(value = "createTime", fill = FieldFill.INSERT)
    private Date createTime = new Date(); //创建时间

    /**
     * 添加文章数据
     * @param a 文章数据
     */
    public void setArticles(Articles a) {
        this.articlesId = a.getArticlesId();
        this.title = a.getTitle();
        this.authorId = a.getAuthorId();
        this.authorName = a.getAuthorName();
    }
}
