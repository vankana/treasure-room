package com.van.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.van.entity.UpFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/22 17:14
 */
@Mapper
public interface UpFileMapper  extends BaseMapper<UpFile>{
}
