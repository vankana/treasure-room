package com.van.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.van.entity.Admin;
import com.van.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/11 10:14
 */
@Mapper
public interface MenuMapper  extends BaseMapper<Menu>{

}