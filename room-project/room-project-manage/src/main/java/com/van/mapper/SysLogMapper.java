package com.van.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.van.entity.Admin;
import com.van.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author VanKa (tel:15228297669 qq:2395073039)
 * @date 2021-05-08 16:41
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {
}
