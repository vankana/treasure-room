package com.van.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.van.entity.Articles;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/19 10:05
 */
@Mapper
public interface ArticlesMapper extends BaseMapper<Articles> {

}