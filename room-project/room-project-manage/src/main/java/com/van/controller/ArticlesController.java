package com.van.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.van.Service.ArticlesCategoryService;
import com.van.Service.ArticlesService;
import com.van.Service.ArticlesTagService;
import com.van.Service.SysLogService;
import com.van.entity.Articles;
import com.van.entity.ArticlesCategory;
import com.van.entity.ResPonObject;
import com.van.entity.SysLog;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigInteger;

/**
 * 文章管理
 * @author VanKa qq:2395073039
 * @date 2021/6/19 10:13
 */
@Controller
@RequestMapping("/articles")
public class ArticlesController {
    @Autowired
    private SysLogService sysLogService;
    @Autowired
    private ArticlesService articlesService;
    @Autowired
    private ArticlesTagService articlesTagService;
    @Autowired
    private ArticlesCategoryService articlesCategoryService;

    /**
     * 获取数据列表
     * @param keyword 模糊查询
     * @param page 页码
     * @param sizes 页面大小
     */
    @ResponseBody
    @RequestMapping("/getList")
    public ResPonObject getList(
            @RequestParam(name = "title", required = false, defaultValue = "") String title,
            @RequestParam(name = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "limit", required = false, defaultValue = "10") int sizes) {
        //todo 判断权限
        QueryWrapper<Articles> queryWrapper = new QueryWrapper<>();

        if (Strings.isNotEmpty(title)) {
            queryWrapper.eq("title", title);
        }
        if (Strings.isNotEmpty(keyword)) {
            queryWrapper.like("title", keyword).or().like("brief", keyword);
        }
        queryWrapper.orderByDesc("createTime");

        IPage<Articles> pMenu = articlesService.page(new Page<>(page,sizes), queryWrapper);

        return new ResPonObject((int) pMenu.getTotal(),pMenu.getRecords());
    }
    /**
     * 根据id获取对象
     * @param id id
     */
    @ResponseBody
    @RequestMapping("/getById")
    public ResPonObject getById(@RequestParam("id") String id) {
        //todo 判断权限
        if (Strings.isEmpty(id)){
            return new ResPonObject("传递参数丢失，查询失败！", ResPonObject.Status.ERROR);
        }
        Articles byId = articlesService.getById(id);

        return new ResPonObject((byId != null), byId,"查询数据为空");
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/del")
    public ResPonObject del(@RequestParam("id") String id) {
        //todo 判断权限
        if (Strings.isEmpty(id)){
            return new ResPonObject("传递参数丢失，查询失败！", ResPonObject.Status.ERROR);
        }

        Articles byId = articlesService.getById(id);
        if (byId == null){
            return new ResPonObject("数据不存在，修改失败！", ResPonObject.Status.ERROR);
        }


        boolean on = articlesService.removeById(id);
        if (on){
            sysLogService.save(new SysLog(SysLog.Status.Del_Articles,"用户[xxx]" + "删除文章[" + byId.getTitle() + "]",byId,1,null));
        }
        //todo 删除tag 修改分类
        return new ResPonObject(on, byId,"删除失败！");
    }
    /**
     * 添加
     */
    @ResponseBody
    @RequestMapping("/add")
    public ResPonObject add(
            @RequestParam(name = "title", required = false, defaultValue = "") String title,
            @RequestParam(name = "content", required = false, defaultValue = "") String content,
            @RequestParam(name = "img", required = false, defaultValue = "") String picture,
            @RequestParam(name = "type", required = false, defaultValue = "-1") int type,
            @RequestParam(name = "label", required = false, defaultValue = "") String label,
            @RequestParam(name = "brief", required = false, defaultValue = "") String brief,
            @RequestParam(name = "categoryId", required = false, defaultValue = "") String categoryId
    ) {
        Articles a = new Articles();

        if (type < 0){
            return new ResPonObject("文章类型错误！", ResPonObject.Status.ERROR);
        }else {
            a.setType(type);
        }

        if (Strings.isEmpty(title)){
            return new ResPonObject("请输入文章名！", ResPonObject.Status.ERROR);
        }else {
            a.setTitle(title);
        }

        if (Strings.isEmpty(picture)){
            a.setPicture("none");
        }else {
            a.setPicture(picture);
        }

        if (Strings.isEmpty(categoryId)){
            a.setCategoryId("0");
            a.setCategoryName("未分类");
        }else {
            ArticlesCategory articlesCategory = articlesCategoryService.getById(categoryId);
            if (articlesCategory == null){
                return new ResPonObject("分类不存在！", ResPonObject.Status.ERROR);
            }
            a.setCategoryId(articlesCategory.getCategoryId());
            a.setCategoryName(articlesCategory.getCategoryName());
        }

        //todo 获取用户信息
        a.setAuthorId("0");
        a.setAuthorName("佚名");

        if (brief.length() <= 30){
            a.setBrief(brief);
        }else {
            a.setBrief(brief.substring(0, 30) + "...");
        }

        a.setContentType(2);
        a.setHtmlContent(content);

        a.setTag(label);

        boolean save = articlesService.save(a);

        if (save){
            articlesTagService.addLabels(label, a);
        }

        return new ResPonObject(save,a,"添加失败");
    }

    /**
     * 添加
     */
    @ResponseBody
    @RequestMapping("/update")
    public ResPonObject add(
            @RequestParam(name = "id", required = false, defaultValue = "") String id,
            @RequestParam(name = "title", required = false, defaultValue = "") String title,
            @RequestParam(name = "content", required = false, defaultValue = "") String content,
            @RequestParam(name = "img", required = false, defaultValue = "") String picture,
            @RequestParam(name = "type", required = false, defaultValue = "-1") int type,
            @RequestParam(name = "label", required = false, defaultValue = "") String label,
            @RequestParam(name = "brief", required = false, defaultValue = "") String brief,
            @RequestParam(name = "categoryId", required = false, defaultValue = "") String categoryId
    ) {
        if (Strings.isEmpty(id)){
            return new ResPonObject("文章id参数丢失！", ResPonObject.Status.ERROR);
        }
        Articles a = articlesService.getById(id);

        if (a == null){
            return new ResPonObject("文章不存在！", ResPonObject.Status.ERROR);
        }

        //todo 判断作者信息
        a.setAuthorId("0");
        a.setAuthorName("佚名");

        if (type < 0){
            return new ResPonObject("文章类型错误！", ResPonObject.Status.ERROR);
        }else {
            a.setType(type);
        }

        if (Strings.isEmpty(title)){
            return new ResPonObject("请输入文章名！", ResPonObject.Status.ERROR);
        }else {
            a.setTitle(title);
        }

        if (Strings.isEmpty(picture)){
            a.setPicture("none");
        }else {
            a.setPicture(picture);
        }

        if (Strings.isEmpty(categoryId)){
            a.setCategoryId("0");
            a.setCategoryName("未分类");
        }else {
            ArticlesCategory articlesCategory = articlesCategoryService.getById(categoryId);
            if (articlesCategory == null){
                return new ResPonObject("分类不存在！", ResPonObject.Status.ERROR);
            }
            a.setCategoryId(articlesCategory.getCategoryId());
            a.setCategoryName(articlesCategory.getCategoryName());
        }

        if (brief.length() <= 30){
            a.setBrief(brief);
        }else {
            a.setBrief(brief.substring(0, 30) + "...");
        }

        a.setContentType(2);
        a.setHtmlContent(content);

        a.setTag(label);

        boolean save = articlesService.updateById(a);

        if (save){
            articlesTagService.addLabels(label, a);
        }

        return new ResPonObject(save,a,"修改失败");
    }
}
