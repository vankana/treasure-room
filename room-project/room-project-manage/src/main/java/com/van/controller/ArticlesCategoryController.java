package com.van.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.van.Service.ArticlesCategoryService;
import com.van.Service.SysLogService;
import com.van.entity.ArticlesCategory;
import com.van.entity.Menu;
import com.van.entity.ResPonObject;
import com.van.entity.SysLog;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 文章分类管理
 * @author VanKa qq:2395073039
 * @date 2021/6/19 10:13
 */
@Controller
@RequestMapping("/category")
public class ArticlesCategoryController {
    @Autowired
    private SysLogService sysLogService;
    @Autowired
    private ArticlesCategoryService articlesCategoryService;

    /**
     * 获取数据列表
     * @param keyword 模糊查询
     * @param page 页码
     * @param sizes 页面大小
     * @return
     */
    @ResponseBody
    @RequestMapping("/getList")
    public ResPonObject getList(
            @RequestParam(name = "categoryName", required = false, defaultValue = "") String categoryName,
            @RequestParam(name = "keyword", required = false, defaultValue = "") String keyword,
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "limit", required = false, defaultValue = "10") int sizes) {
        //todo 判断权限
        QueryWrapper<ArticlesCategory> queryWrapper = new QueryWrapper<>();

        if (Strings.isNotEmpty(categoryName)) {
            queryWrapper.eq("categoryName", categoryName);
        }
        if (Strings.isNotEmpty(keyword)) {
            queryWrapper.like("categoryName", keyword).or().like("remark", keyword);
        }
        queryWrapper.orderByDesc("createTime");

        IPage<ArticlesCategory> pMenu = articlesCategoryService.page(new Page<>(page,sizes), queryWrapper);

        return new ResPonObject((int) pMenu.getTotal(),pMenu.getRecords());
    }
    /**
     * 根据id获取对象
     * @param id id
     */
    @ResponseBody
    @RequestMapping("/getById")
    public ResPonObject getById(@RequestParam("id") String id) {
        //todo 判断权限
        if (Strings.isEmpty(id)){
            return new ResPonObject("传递参数丢失，查询失败！", ResPonObject.Status.ERROR);
        }
        ArticlesCategory byId = articlesCategoryService.getById(id);

        return new ResPonObject((byId != null), byId,"查询数据为空");
    }
    /**
     * 添加
     */
    @ResponseBody
    @RequestMapping("/add")
    public ResPonObject add(
            @RequestParam(name = "name", required = false, defaultValue = "") String name,
            @RequestParam(name = "remark", required = false, defaultValue = "") String remark
    ) {
        //todo 判断权限

        QueryWrapper<ArticlesCategory> queryWrapper = new QueryWrapper<>();
        if (Strings.isNotEmpty(name)){
            queryWrapper.eq("categoryName", name);
        }else {
            return new ResPonObject("请输入标题！", ResPonObject.Status.ERROR);
        }
        List<ArticlesCategory> list = articlesCategoryService.list(queryWrapper);

        if (list.size() > 0){
            return new ResPonObject("标题重复！", ResPonObject.Status.ERROR);
        }
        ArticlesCategory a = new ArticlesCategory();
        a.setCategoryName(name);
        a.setRemark(remark);
        boolean save = articlesCategoryService.save(a);
        if (save){
            sysLogService.save(new SysLog(SysLog.Status.Add_Articles_Category,"用户[xxx]" + "添加文章分类[" + name + "]",a,1,null));
        }
        return new ResPonObject(save,a,"添加失败");
    }
    /**
     * 添加
     */
    @ResponseBody
    @RequestMapping("/update")
    public ResPonObject update(
            @RequestParam(name = "id", required = false, defaultValue = "") String id,
            @RequestParam(name = "name", required = false, defaultValue = "") String name,
            @RequestParam(name = "remark", required = false, defaultValue = "") String remark
    ) {
        //todo 判断权限
        if (Strings.isEmpty(id)){
            return new ResPonObject("传递参数丢失，修改失败！", ResPonObject.Status.ERROR);
        }
        ArticlesCategory byId = articlesCategoryService.getById(id);
        if (byId == null){
            return new ResPonObject("数据不存在，修改失败！", ResPonObject.Status.ERROR);
        }

        QueryWrapper<ArticlesCategory> queryWrapper = new QueryWrapper<>();
        if (Strings.isNotEmpty(name)){
            queryWrapper.eq("categoryName", name);
        }else {
            return new ResPonObject("请输入标题！", ResPonObject.Status.ERROR);
        }
        boolean b = articlesCategoryService.QueryDataDuplicated(id, queryWrapper);
        if(!b){
            return new ResPonObject("标题重复！", ResPonObject.Status.ERROR);
        }
        byId.setCategoryName(name);
        byId.setRemark(remark);
        boolean on = articlesCategoryService.updateById(byId);
        if (on){
            sysLogService.save(new SysLog(SysLog.Status.Update_Articles_Category,"用户[xxx]" + "修改文章分类[" + name + "]",byId,1,null));
        }
        return new ResPonObject(on,byId,"修改失败");
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/del")
    public ResPonObject del(@RequestParam("id") String id) {
        //todo 判断权限
        if (Strings.isEmpty(id)){
            return new ResPonObject("传递参数丢失，查询失败！", ResPonObject.Status.ERROR);
        }

        ArticlesCategory byId = articlesCategoryService.getById(id);
        if (byId == null){
            return new ResPonObject("数据不存在，修改失败！", ResPonObject.Status.ERROR);
        }

        boolean on = articlesCategoryService.removeById(id);

        if (on){
            sysLogService.save(new SysLog(SysLog.Status.Del_Articles_Category,"用户[xxx]" + "删除文章分类[" + byId.getCategoryName() + "]",byId,1,null));
        }

        return new ResPonObject(on, byId,"删除失败！");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @ResponseBody
    @RequestMapping("/dels")
    public ResPonObject dels(@RequestParam("ids[]") String[] ids) {
        boolean on = false;
        String msg = "", err = "", suc = "";

        //todo 判断权限

        if (ids.length > 0){
            for (String i : ids){
                boolean b = articlesCategoryService.removeById(i);
                if (b){
                    suc += " [" + i + "] ";
                }else {
                    on = true;
                    err += " [" + i + "] ";
                }
            }
            msg = (err.length() > 0 ? "删除文章分类" + err +"（编号）失败！\n" :"") + (suc.length() >0 ? "删除文章分类" + suc +"（编号）成功！" :"");

            sysLogService.save(new SysLog(SysLog.Status.Del_Articles_Category,"用户[xxx]" + msg, msg,null));

            return new ResPonObject(!on, null,msg);
        }

        return new ResPonObject(false, null,"请选中数据");
    }
}
