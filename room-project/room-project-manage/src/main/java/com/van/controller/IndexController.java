package com.van.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author VanKa qq:2395073039
 * @date 2021/6/4 17:07
 */

@Controller
@RequestMapping("/index")
public class IndexController {

    @RequestMapping("/index")
    public String indexCome(
            HttpServletRequest request) {
        return "index";
    }

    @ResponseBody
    @RequestMapping("/hi")
    public String index(
            HttpServletRequest request) {
        String s = "莲花云泊更新啦！\n新增实名认证，认证后可以享受超值优惠";
        return "你好！这里是 room-poject-tourist 基于spring cloud alibaba";
    }
}
