package com.van.controller;

import com.van.entity.ResPonObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 初始数据
 * @author VanKa qq:2395073039
 * @date 2021/6/18 9:16
 */

@Controller
@RequestMapping("/info")
public class InfoController {
    /**
     * 获取管理员数据
     * @return
     */
    @ResponseBody
    @RequestMapping("/admin")
    public ResPonObject admin(){
        Map<String, Object> params = new HashMap<>();
        params.put("name","测试");
        return new ResPonObject(params);
    }
}
