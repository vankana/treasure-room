package com.van.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.van.Service.SysLogService;
import com.van.entity.Admin;
import com.van.entity.ResPonObject;
import com.van.entity.SysLog;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日志管理
 * @author VanKa qq:2395073039
 * @date 2021/6/10 11:33
 */
@Controller
@RequestMapping("/log")
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    /**
     * 获取数据列表
     * @param keyword 模糊查询
     * @param page 页码
     * @param sizes 页面大小
     * @return
     */
    @ResponseBody
    @RequestMapping("/getList")
    public ResPonObject getList(
                             @RequestParam(name = "keyword", required = false, defaultValue = "") String keyword,
                             @RequestParam(name = "page", required = false, defaultValue = "1") int page,
                             @RequestParam(name = "limit", required = false, defaultValue = "10") int sizes) {
        //todo 判断权限
        QueryWrapper<SysLog> queryWrapper = new QueryWrapper<>();
        //todo 添加查询条件
        if (!Strings.isNotEmpty(keyword)) {
            queryWrapper.like("content", keyword).or().like("title", keyword);
        }
        queryWrapper.orderByDesc("createTime");
        Page<SysLog> logPage = sysLogService.page(new Page<>(page, sizes), queryWrapper);

        return new ResPonObject((int) logPage.getTotal(),logPage.getRecords());
    }

    /**
     * 根据id获取对象
     * @param id id
     * @return
     */
    @ResponseBody
    @RequestMapping("/getById")
    public ResPonObject getById(@RequestParam("id") int id) {
        //todo 判断权限
        SysLog byId = sysLogService.getById(id);

        return new ResPonObject((byId != null), byId,"查询数据为空");
    }

}
