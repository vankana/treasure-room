package com.van.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.van.Service.AdminService;
import com.van.Service.SysLogService;
import com.van.entity.Admin;
import com.van.entity.ResPonObject;
import com.van.entity.SysLog;
import com.van.utils.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/7 14:45
 */
@Controller
@RequestMapping("/login")
public class LoginController {
    @Value("${customConfig.adminMD5Key}")
    private String adminMD5Key;
    @Autowired
    private AdminService adminService;
    @Autowired
    private SysLogService sysLogService;


    //todo 日志

    /**
     * 登录
     * @param account 账号
     * @param password 密码
     */
    @ResponseBody
    @RequestMapping("/enter")
    public ResPonObject indexCome(
            HttpServletRequest request,
            @RequestParam("account") String account,
            @RequestParam("password") String password
    ) {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account",account);
        Admin one = adminService.getOneData(queryWrapper);
        if (one == null){
            return new ResPonObject("未查询到用户信息", ResPonObject.Status.ERROR);
        }
        //todo 累计3次输入手机号获取验证码

        if (!one.getPassword().equals(MD5Util.MD5Encode(password,adminMD5Key))){
            return new ResPonObject("密码错误！", ResPonObject.Status.ERROR);
        }
        //todo 存储用户session

        one.setLastLoginTime(new Date());
        adminService.updateById(one);

        sysLogService.save(new SysLog(SysLog.Status.LOGIN,"用户[" + one.getName()+ "]" + "登录系统",one,1,one));

        return new ResPonObject();
    }
}
