package com.van;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.van.mapper")
public class RoomProjectManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoomProjectManageApplication.class, args);
    }

}
