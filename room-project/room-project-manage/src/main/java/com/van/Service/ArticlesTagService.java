package com.van.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.van.entity.Articles;
import com.van.entity.ArticlesTag;

public interface ArticlesTagService extends IService<ArticlesTag> {
    /**
     * 查询指定数据数据
     * @param q 查询内容
     * @return 单个数据
     */
    ArticlesTag getOneData(QueryWrapper<ArticlesTag> q);

    boolean addLabels(String s, Articles a);
}
