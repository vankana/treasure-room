package com.van.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.van.entity.Admin;
import com.van.entity.Menu;

import java.util.List;

public interface MenuService extends IService<Menu> {
    /**
     * 查询指定数据数据
     * @param queryWrapper 查询内容
     * @return 单个数据
     */
    Menu getOneData(QueryWrapper<Menu> queryWrapper);

    /**
     * 获取排序整理后的导航列表
     * @param lists 导航列表
     * @return 导航列表
     */
    List<Menu> organize(List<Menu> lists);

    /**
     * 获取排序整理后的导航列表
     * @param queryWrapper 查询内容
     * @return 导航列表
     */
    List<Menu> organizeList(QueryWrapper<Menu> queryWrapper);
}
