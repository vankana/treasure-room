package com.van.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.van.Service.SysLogService;
import com.van.entity.SysLog;
import com.van.mapper.SysLogMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/9 22:11
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

    @Override
    public SysLog getOneData(QueryWrapper<SysLog> queryWrapper) {
        List<SysLog> list = list(queryWrapper);
        if (list.size() == 1) {
            return list.get(0);
        } else {
            return null;
        }
    }
}