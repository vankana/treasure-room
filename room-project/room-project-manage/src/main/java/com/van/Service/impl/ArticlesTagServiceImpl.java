package com.van.Service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.van.Service.ArticlesTagService;
import com.van.entity.Articles;
import com.van.entity.ArticlesTag;
import com.van.mapper.ArticlesTagMapper;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/9 14:46
 */
@Service
public class ArticlesTagServiceImpl extends ServiceImpl<ArticlesTagMapper, ArticlesTag> implements ArticlesTagService {

    @Override
    public ArticlesTag getOneData(QueryWrapper<ArticlesTag> q) {
        List<ArticlesTag> list = list(q);
        if (list.size() == 1){
            return list.get(0);
        }else {
            return null;
        }
    }

    @Override
    public boolean addLabels(String s, Articles a) {
        if (Strings.isEmpty(s)){
            return false;
        }
        if (a == null){
            return false;
        }
        QueryWrapper<ArticlesTag> q = new QueryWrapper<>();
        q.eq("articlesId",a.getArticlesId());
        remove(q);
        String[] ss = s.split(",");
        for(String i : ss){
            ArticlesTag tag = new ArticlesTag();
            tag.setTagName(i);
            tag.setArticles(a);
            save(tag);
        }
        return true;
    }
}
