package com.van.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.van.entity.Admin;

public interface AdminService extends IService<Admin> {
    /**
     * 查询指定数据数据
     * @param queryWrapper 查询内容
     * @return 单个数据
     */
    Admin getOneData(QueryWrapper<Admin> queryWrapper);
}
