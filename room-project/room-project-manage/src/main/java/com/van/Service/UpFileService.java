package com.van.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.van.entity.UpFile;

import java.util.List;

public interface UpFileService extends IService<UpFile> {
}
