package com.van.Service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.van.Service.AdminService;
import com.van.entity.Admin;
import com.van.mapper.AdminMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/9 14:46
 */
@Service
public class AdminServiceImpl  extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Override
    public Admin getOneData(QueryWrapper<Admin> queryWrapper) {
        List<Admin> list = list(queryWrapper);
        if (list.size() == 1){
            return list.get(0);
        }else {
            return null;
        }
    }
}
