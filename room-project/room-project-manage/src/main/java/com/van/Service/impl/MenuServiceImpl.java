package com.van.Service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.van.Service.MenuService;
import com.van.entity.Menu;
import com.van.mapper.MenuMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Override
    public Menu getOneData(QueryWrapper<Menu> queryWrapper) {
        List<Menu> list = list(queryWrapper);
        if (list.size() == 1){
            return list.get(0);
        }else {
            return null;
        }
    }
    @Override
    public List<Menu> organize(List<Menu> lists){

        List<Menu> superior = new ArrayList<>();//上级
        List<Menu> subordinate = new ArrayList<>();//下级
        List<Menu> date = new ArrayList<>();//最终结果

        for (Menu menu : lists){
            if (menu.getSId() == 0){
                superior.add(menu);
            }else {
                subordinate.add(menu);
            }
        }

        for (Menu menu : superior){
            List<Menu> arr = new ArrayList<>();
            for (Menu m : subordinate){
                if(m.getSId() == menu.getId()){
                    arr.add(m);
                }
            }
            menu.setData(arr);
            date.add(menu);
        }

        return date;
    }

    @Override
    public List<Menu> organizeList(QueryWrapper<Menu> queryWrapper) {
        List<Menu> list = null;
        if (queryWrapper == null){
            list = list();
        }else {
            list = list(queryWrapper);
        }

        if (list.size() > 0){
            return organize(list);
        }else {
            return null;
        }
    }
}
