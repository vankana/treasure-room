package com.van.Service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.van.Service.ArticlesCategoryService;
import com.van.entity.ArticlesCategory;
import com.van.mapper.ArticlesCategoryMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/9 14:46
 */
@Service
public class ArticlesCategoryServiceImpl extends ServiceImpl<ArticlesCategoryMapper, ArticlesCategory> implements ArticlesCategoryService {

    @Override
    public ArticlesCategory getOneData(QueryWrapper<ArticlesCategory> queryWrapper) {
        List<ArticlesCategory> list = list(queryWrapper);
        if (list.size() == 1){
            return list.get(0);
        }else {
            return null;
        }
    }

    @Override
    public boolean QueryDataDuplicated(String id, QueryWrapper<ArticlesCategory> queryWrapper) {
        List<ArticlesCategory> list = list(queryWrapper);
        if (list.size() == 0){
            return true;
        }else if (list.size() == 1){
            if(id.equals(list.get(0).getCategoryId())){
                return true;
            }
        }
        return false;
    }
}
