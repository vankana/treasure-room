package com.van.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.van.entity.ArticlesCategory;

public interface ArticlesCategoryService extends IService<ArticlesCategory> {
    /**
     * 获取/判断唯一数据
     * @param queryWrapper 查询内容
     * @return 单个数据
     */
    ArticlesCategory getOneData(QueryWrapper<ArticlesCategory> queryWrapper);

    /**
     * 判断数据是否重复
     * @param id 通过判断id是否重复
     * @param queryWrapper 查询内容
     * @return 结果 true 非重 false 重
     */
    boolean QueryDataDuplicated(String id,QueryWrapper<ArticlesCategory> queryWrapper);
}
