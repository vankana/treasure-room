package com.van.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.van.Service.UpFileService;
import com.van.entity.UpFile;
import com.van.mapper.UpFileMapper;
import org.springframework.stereotype.Service;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/22 17:14
 */
@Service
public class UpFileServiceImpl  extends ServiceImpl<UpFileMapper, UpFile> implements UpFileService {
}
