package com.van.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.van.entity.Articles;

import java.util.List;

public interface ArticlesService extends IService<Articles> {
    /**
     * 查询指定数据数据
     * @param queryWrapper 查询内容
     * @return 单个数据
     */
    Articles getOneData(QueryWrapper<Articles> queryWrapper);
}
