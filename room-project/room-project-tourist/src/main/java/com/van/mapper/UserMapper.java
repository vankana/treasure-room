package com.van.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.van.entity.user.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/4 14:52
 */
@Mapper
public interface UserMapper  extends BaseMapper<User>{
}
