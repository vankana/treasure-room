package com.van.entity.article;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章摘要
 * @author VanKa (tel:15228297669 qq:2395073039)
 * @date 2021-04-17 19:55
 */
@Data
@TableName(value = "article_brief")
public class ArticleBrief implements Serializable {
    /**
     * 文章Id 摘要ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 文章状态 0 存档 1 发布
     */
    private Integer type = 0;
    /**
     * 文章类型 0 原创 1 转载
     */
    private Integer state = 0;
    /**
     * 文章章摘要图片
     */
    private String picture;
    /**
     * 文章简介
     */
    private String brief;

    /**
     * 文章浏览量
     */
    private Integer views = 0;
    /**
     * 文章转发量
     */
    private Integer shares = 0;
    /**
     * 文章点赞量
     */
    private Integer likes = 0;

    /**
     * 文章创建时间
     */
    @TableField(value = "createTime", fill = FieldFill.INSERT)
    private Date createTime = new Date();
    /**
     * 修改、发布时间
     */
    @TableField(value = "modifyTime", fill = FieldFill.UPDATE)
    private Date modifyTime = new Date();

    /**
     * 作者ID
     */
    @TableField(value = "authorId")
    private Integer authorId;
    /**
     * 作者昵称
     */
    @TableField(value = "authorName")
    private String authorName;

    /**
     * 文章标签
     */
    private String label;
    /**
     * 分类
     */
    private String sort;
    @TableField(value = "sortId")
    private int sortId = 0;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer deleted = 0;
}
