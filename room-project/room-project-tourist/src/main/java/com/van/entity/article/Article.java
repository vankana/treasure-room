package com.van.entity.article;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 文章实体类
 */
@Data
@TableName(value = "article")
public class Article implements Serializable{
    /**
     * 文章Id 摘要ID
     */
    @TableId(value = "id")
    private String id;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer deleted = 0;

    public void setBrief(ArticleBrief brief) {
        this.id = brief.getId();
        this.title = brief.getTitle();
    }
    public void setBrief(ArticleBrief brief,String s) {
        this.id = brief.getId();
        this.title = brief.getTitle();
        this.content = s;
    }
}
