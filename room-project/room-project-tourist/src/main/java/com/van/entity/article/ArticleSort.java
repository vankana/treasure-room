package com.van.entity.article;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 文章分类
 * @author VanKa (tel:15228297669 qq:2395073039)
 * @date 2021-04-15 16:05
 */
@Data
@TableName(value = "article_sort")
public class ArticleSort {
    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    private String name;

}
