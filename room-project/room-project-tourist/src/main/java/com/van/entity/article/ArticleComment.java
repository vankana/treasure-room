package com.van.entity.article;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 文章评论
 * @author VanKa (tel:15228297669 qq:2395073039)
 * @date 2021-04-17 20:01
 */

@Data
@TableName(value = "article_comment")
public class ArticleComment {
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;
    private String userId;
    private String userName;
    /**
     * 评论内容
     */
    private String content;

    /**
     * 头像
     */
    private String avatar;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 简介id
     */
    private String BriefId;
    private ArticleBrief blog;
    /**
     * 文章id
     */
    private String articleId;
    /**
     *
     */
    private Long parentCommentId;
    private String parentNickname;

    /**
     * 评论上级 0无上级
     */
    private ArticleComment parentId;
    /**
     * 评论下级
     */
    private List<ArticleComment> replyComments = new ArrayList<>();
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer deleted;
}
