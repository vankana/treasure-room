package com.van.entity.user;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户列表
 * 之后得把用户微信数据独立出来
 */
@Data
@TableName(value = "admin")
public class Admin implements Serializable {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Integer id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别 0.未知 1.男 2.女
     */
    private int sex = 0;
    /**
     * 电话号码
     */
    private String phone;
    /**
     * 头像
     */
    @TableField(value = "headImage")
    private String headImage;
    /**
     * 微信openId
     */
    @TableField(value = "openId")
    private String openId;
    /**
     * 密码
     */
    private String password;
    /**
     * 状态 0.正常使用 1.禁止使用 2.待审核
     */
    private int status = 0;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 创建时间
     */
    @TableField(value = "createTime", fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 上次登录时间
     */
    @TableField(value = "lastLoginTime", fill = FieldFill.UPDATE)
    private Date lastLoginTime;
    /**
     * 备注
     */
    private String remark;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer deleted;
}
