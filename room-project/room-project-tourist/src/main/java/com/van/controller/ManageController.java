package com.van.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 后台管理
 * @author VanKa qq:2395073039
 * @date 2021/6/4 16:58
 */
@Controller
@RequestMapping("/manage")
public class ManageController {
    @RequestMapping("/")
    public String come(
            HttpServletRequest request) {
        return "manage/index";
    }
    @RequestMapping("/index")
    public String indexCome(
            HttpServletRequest request) {
        return "manage/index";
    }
}
