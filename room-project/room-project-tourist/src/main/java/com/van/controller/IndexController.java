package com.van.controller;

import com.van.entity.user.User;
import com.van.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author VanKa qq:2395073039
 * @date 2021/6/3 11:26
 */

@Controller
public class IndexController {
    @Autowired
    private UserService userService;
    @RequestMapping("/")
    public String come(
            HttpServletRequest request) {
        return "index";
    }
    @RequestMapping("/index")
    public String indexCome(
            HttpServletRequest request) {
        return "index";
    }

    @ResponseBody
    @RequestMapping("/hi")
    public String index(
            HttpServletRequest request) {
        return "你好！这里是 room-poject-tourist 基于spring cloud alibaba";
    }

    @ResponseBody
    @RequestMapping("/get/all")
    public String getAll(
            HttpServletRequest request) {
        List<User> list = userService.list();
        String msg = "";
        msg = "查询数据大小" + list.size();
        return "user " + msg;
    }
}
