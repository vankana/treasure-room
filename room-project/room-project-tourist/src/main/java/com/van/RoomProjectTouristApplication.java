package com.van;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomProjectTouristApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoomProjectTouristApplication.class, args);
    }

}
