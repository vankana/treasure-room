package com.van.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.van.service.UserService;
import com.van.entity.user.User;
import com.van.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
 * @author VanKa qq:2395073039
 * @date 2021/6/4 14:55
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
