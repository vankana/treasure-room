package com.van.service;

//import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.IService;
import com.van.entity.user.User;

public interface UserService extends IService<User> {
}
